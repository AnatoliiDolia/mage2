<?php
namespace Dolia\Module\Block;

use Magento\Catalog\Block\Product\ListProduct;


class SmartSearch extends \Magento\Catalog\Block\Product\AbstractProduct

{
    protected $_productCollectionFactory;
    protected $_productVisibility;
    protected $categoryRepository;
    protected $_storeManager;


    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Block\Product\ListProduct $listProductBlock,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
         \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    )
    {
        $this->_storeManager = $storeManager;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->listProductBlock = $listProductBlock;
        $this->categoryRepository = $categoryRepository;
        parent::__construct($context, $data);
    }

    public function getProductCollection($categoryId=null)
    {
         $categoryId = 3;
         $category = $this->categoryRepository->get($categoryId);
                $collection = $this->_productCollectionFactory->create();
                $collection->addAttributeToSelect('*');
                $collection->addCategoryFilter($category);
                $collection->setPageSize(10);


                $pubmedia=$this->_storeManager->getStore()->getBaseMediaDir();
                return $collection;



    }

    /*public function getAddToCartPostParams($product)
    {
        return $this->listProductBlock->getAddToCartPostParams($product);
    }*/


 public function getNewArrival()
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter('new_arrival', 1);
        $collection->setPageSize(10);
      //  $newarrival=print_r($this->_storeManager->getStore()->getUrl('newarrival'));
        return $collection;
    }

    public function getRefurb()
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter('refurb', 1);
        $collection->setPageSize(10);
      //  $variable=print_r($this->_storeManager->getStore()->getUrl('variable'));
        return $collection;


    }


    public function getBaseMediaDir() {
            return $this->_storeManager->getStore()->getBaseMediaDir();
        }


   }
