<?php
namespace Dolia\Module\Block;

use Magento\Catalog\Block\Product\ListProduct;


class variable extends \Magento\Framework\View\Element\Template

{
    protected $_productCollectionFactory;
    protected $_productVisibility;
    protected $categoryRepository;
    protected $_storeManager;


    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Block\Product\ListProduct $listProductBlock,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,


        array $data = []
    )
    {
        $this->_storeManager = $storeManager;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->listProductBlock = $listProductBlock;
        $this->categoryRepository = $categoryRepository;

        parent::__construct($context, $data);
    }

    public function getProductCollection($categoryId = 4)
    {

        $categoryId = $this->getPosition();

        $category = $this->categoryRepository->get($categoryId);

        $collection = $this->_productCollectionFactory->create();

        $collection->addAttributeToSelect('*');

        $collection->addCategoryFilter($category);

        $collection->setPageSize(5);

        return $collection;
    }

    public function getAddToCartPostParams($product)
    {
        return $this->listProductBlock->getAddToCartPostParams($product);
    }

    public function getBaseMediaDir() {
        return $this->_storeManager->getStore()->getBaseMediaDir();
    }




}

