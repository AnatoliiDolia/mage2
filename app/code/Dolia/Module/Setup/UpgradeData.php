<?php
namespace Dolia\Module\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

class UpgradeData implements UpgradeDataInterface {

    private $eavSetupFactory;

    public function __construct( \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory ) {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function upgrade( ModuleDataSetupInterface  $setup, ModuleContextInterface  $context ) {


        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        if(version_compare( $context->  getVersion(), '1.0.3', '<'))
        {
            $setup->startSetup();

            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'refurb', [
                    'type' => 'int',
                    'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                    'frontend' => '',
                    'label' => 'Refurb',
                    'input' => 'boolean',
                    'group' => 'General',
                    'source' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => true,
                    'user_defined' => false,
                    'default' => '1',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false
                ]
            );
            $setup->endSetup();
        }

    }

}
?>